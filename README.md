# Precision-CI


## Overview
This system is designed as a Continuous Integration and Delivery Automation system. It will automatically assist developers with building, testing, and deploying a [sample web application](https://bitbucket.org/brian_mcbrayer/samplewebapi). This repository contains code that can, using PowerShell, provision a bare *Windows 2012 R2* machine with an entire CI delivery system.  
It uses PowerShell to install:  
   
```
  * Git For Windows
  * Jenkins, running on port 8080 
  * Visual Studio Express 2013 for Web
  * NUnit-3
  * Nuget
  * Local IIS Server on port 80
```
_You should provision about 60GB of disk space as this includes Visual Studio components_   

Once those above components are on the server, the following items will be automatically  
injected into the Jenkins systems:
```  
  * Jenkins seed job `data\samplewebapi.xml`  
  * MSBuild publish file `data\SampleWebAPI.pubxml`  
  * nUnit -> jUnit XSL Translation file `scripts\nunit-junit.xslt`  
```

## Jenkins
Jenkins is used to support the Continuous Integration, Testing, and Deployment for this project.
More about Jenkins, how the seed job is included, and the Jenkins Pipeline can be found below.

## Jenkins Seed Job
The Jenkins seed job is fairly simple.  It's sole purpose is to instruct Jenkins  
to obtain the `Jenkinsfile` located at the root of the Bitbucket repo `DrewEasley`.  

It is this Jenkinsfile that works the magic of Pipeline scripting.   See the Pipeline  
section below for more details.

## MSBuild Publish File
The publish file is fairly simple, and instructs msbuild to generate a folder with
all of our files needed to deploy this system into IIS.   

Next steps in this project would be to adjust the deploy process to include an Azure deploy target.
More information about the deploy process is below.

## nUnit Tests
This solution file uses nUnit, and all tests are run by Jenkins.  Jenkins supports the
jUnit.xml file, so a translation file is included with this project.  This will ensure that
Test Results can be easily cataloged by Jenkins. Check out the "Test Results" section on the left
hand side of any Jenkins build job!

## Deploy
When the build job is successful, it will be automatically deployed to an IIS server on the same machine as the Jenkins server.  An IIS .NET application will be created that follows the format of http://ServerName/Build%BuildNumber%  
Example: For build #1: http://precise-ci.juneeighteen.com/Build1  
Note, that the IIS server runs on port 80, not 8080 like Jenkins.

## Build Pipeline
You can view the source of the Pipeline from the .\JenkinsFile in this repository. The pipeline includes the following stages:  
```
    1) Checkout from SCM
    2) Restore Nuget Packages
    3) Build the Solution
    4) Run all nUnit Tests
    5) Deploy to a local IIS folder for testing
```

## Jenkins Plugins
You can see what Jenkins plugins will be installed automatically in the `data\plugins.txt` file.  The PowerShell scripts will download the latest plugin from Jenkins update center servers, and install it.  See the "Next Step" notes about pinning Jenkins plugin versions.

## Quick Start
1. Launch a clean Windows Server 2012 R2 image.  
2. Install git
   ```
   Set-ExecutionPolicy Bypass  
   (New-Object System.Net.WebClient).DownloadFile("https://bitbucket.org/DrewEasley/precise-ci/raw/master/scripts/install-git.ps1", "install-git.ps1")   
   .\install-git.ps1  
   ```  
3. Close, then re-open PowerShell to access git and clone the repository.  
4. Clone the repository, and run the `install-server-components.ps1` script.
   ```
   git clone https://bitbucket.org/DrewEasley/precise-ci  
   cd precise-ci  
   .\scripts\install-server-components.ps1  
   ```

**Jenkins, Visual Studio, and more are being automatically provisioned  at this point.**
**You may want to snapshot your virtual machine at this point.**

1. You may now access Jenkins at http://SERVER:8080 or http://precise-ci.juneeighteen.com:8080  
Login with the username 'admin' and the password in the file `C:\Program Files (x86)\Jenkins\secrets\initialAdminPassword`

2. Build the job named 'simplewebapi'`

3. Visit http://SERVER/Build1 or http://precise-ci.juneeighteen.com/Build1/api/product once the job completes successfully

## Next Steps (not yet done)

    * A code signing certificate should be created so Bypassing the Execution policy is not necessary
    * Design a deploy to MS Azure instead of a local IIS server
    * Test on multiple build servers (like Server 2016)
    * Add functionality that can pin Jenkins plugins to specific versions
    * Tear down older IIS Applications so we don't have an infinite number running in IIS
    * Job Parameters to allow branch builds, and not just master build
    * Better Jenkins Security (perhaps using BitBucket SSO)
    * Enable the Jenkins build parameter to monitor the repo for changes, build automatically on changes
    * Email the developer who submitted the above changes a link to their test environment

## Requirements
        1) Windows Server with internet access (Server 2012 R2 was used for this project)
        2) Git for Windows
        3) PowerShell