# Author: Drew Easley

# This script will bootstrap Git-for-windows onto the Windows machine


# (New-Object System.Net.WebClient).DownloadFile("https://bitbucket.org/DrewEasley/precise-ci/raw/master/scripts/install-git.ps1", "install-git.ps1")
# Set-ExecutionPolicy Bypass
# .\install-git.ps1



function Install-Git {
    # Create a temporary directory
    $parent = [System.IO.Path]::GetTempPath()
    [string] $name = [System.Guid]::NewGuid()
    $tempWorkspace = New-Item -ItemType Directory -Path (Join-Path $parent $name)

    #Download Git
    $downloadTo = Join-Path $tempWorkspace "git-for-windows.exe"
    (New-Object System.Net.WebClient).DownloadFile("https://github.com/git-for-windows/git/releases/download/v2.10.1.windows.1/Git-2.10.1-32-bit.exe", $downloadTo)

    #Install Git
    Start-Process $downloadTo  -ArgumentList "/SILENT /LOG='git-for-win-install.log'" -wait
}

Install-Git
