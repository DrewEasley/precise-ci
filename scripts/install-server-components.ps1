# Author: Drew Easley
# This script will bootstrap a Windows Server 2012 machine with ..
# ... git
# ... Jenkins
# ... Jenkins Plugins
# ... Visual Studio 2013 Express for Web


#Configuration Settings

#TODO: define var names better
#Last tested with Jenkins 2.25
$jenkinsURL = "http://mirrors.jenkins-ci.org/windows/jenkins-2.25.zip"

#What version of Nuget to use?
$nugetURL = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"

#Warning. Does not yet support NUnit 3.5! 3.5 moved the nunit-console command into a separate repo
$nunitURL = "https://github.com/nunit/nunit/releases/download/3.4.1/NUnit-3.4.1.zip"

#Must use VS 2013 WEB (not Desktop)
$vsExpressURL = "https://download.microsoft.com/download/2/3/9/239474C9-8470-4A71-8D77-6C972A49DF0A/vns_full.exe"



# Create a temporary workspace to download and unzip file
function New-TemporaryDirectory {
    $parent = [System.IO.Path]::GetTempPath()
    [string] $name = [System.Guid]::NewGuid()
    New-Item -ItemType Directory -Path (Join-Path $parent $name)
}


# Download a file
function Download-File {
    Param(
        [string]$downloadUrl,
        [string]$downloadAs,
        [string]$workspaceFolder
        )

    $downloadTo = Join-Path $workspaceFolder $downloadAs
    echo "Downloading $downloadUrl"
    (New-Object System.Net.WebClient).DownloadFile($downloadUrl, $downloadTo)
}

# Unzip a file
function Unzip-File {
    Param(
        [string]$zipFile,
        [string]$extractFolder,
        [string]$workspaceFolder
    )
    Add-Type -assembly "system.io.compression.filesystem"
    $unzipFrom=Join-Path $workspaceFolder $zipFile
    $unzipTo=Join-Path $workspaceFolder $extractFolder
    [io.compression.zipfile]::ExtractToDirectory($unzipFrom, $unzipTo)
}

# Wrapper to silently install a .exe file
function Silent-ExeInstall {
    Param(
        [string]$installerFile,
        [string]$workspaceFolder
    )
    $installerLocation=Join-Path $tempWorkspace $installerFile
    $installLog=$installerLocation+".log"
    Start-Process $installerLocation  "/Passive /Log $installLog"
}

# Wrapper to silently install a .msi file
function Silent-MsiInstall {
    Param(
        [string]$installerFile,
        [string]$workspaceFolder
    )
    $installerLocation=Join-Path $tempWorkspace $installerFile
    $installLog=$installerLocation+".log"

    Start-Process "msiexec.exe" -ArgumentList "/i $installerLocation /passive /l* $installLog" -wait
}

function Install-Jenkins-Plugin {
    Param(
        [string]$pluginName,
        [string]$jenkinsHome
    )
    $pluginRoot = Join-Path $jenkinsHome "plugins"
    Download-File https://updates.jenkins-ci.org/latest/${pluginName}.hpi "${pluginName}.hpi" $pluginRoot
}

function Install-Jenkins-Plugins {
    Param(
        [string]$jenkinsHome
    )
    $jenkinsPlugins = Get-Content data/plugins.txt
    foreach ($jPlugin in $jenkinsPlugins) {Install-Jenkins-Plugin $jPlugin $jenkinsHome}

}

function Install-Jenkins {
    Param(
        [string]$jenkinsURL,
        [string]$workspaceFolder
    )
    Download-File $jenkinsURL "jenkins.zip" $workspaceFolder
    Unzip-File "jenkins.zip" "jenkins-installer" $workspaceFolder
    Silent-MsiInstall "jenkins-installer\jenkins.msi" $workspaceFolder

    $jenkinsService = Get-WmiObject win32_service | ?{$_.Name -like 'Jenkins'}
    $jenkinsHome = (Split-Path $jenkinsService.PathName).Trim('"')

    Stop-Service Jenkins
    echo "2.0" >> (Join-Path $jenkinsHome "jenkins.install.InstallUtil.lastExecVersion")
    Install-Jenkins-Plugins $jenkinsHome
    Start-Service Jenkins

}

function Install-VSExpress {
    Param(
        [string]$workspaceFolder
    )

    Download-File $vsExpressURL "wdexpress_full.exe" $workspaceFolder
    Silent-ExeInstall "wdexpress_full.exe" $workspaceFolder

    #Download-File "https://download.microsoft.com/download/9/0/B/90B58F0F-4653-4E36-A6F2-4AB8EDD994E9/WSE2012SDKInstaller.msi" "WSE2012SDKInstaller.msi" $workspaceFolder
    #Silent-MsiInstall "WSE2012SDKInstaller.msi" $workspaceFolder

}

function Install-NUnit {
    Download-File $nunitURL "nunit.zip" "C:\Program Files"
    Unzip-File "nunit.zip" "nunit" "C:\Program Files\"
}

function Install-Nuget {
    Download-File $nugetURL "nuget.exe" "C:\Program Files"
}


function Jenkins-SeedJob {
    $jenkinsService = Get-WmiObject win32_service | ?{$_.Name -like 'Jenkins'}
    $jenkinsHome = (Split-Path $jenkinsService.PathName).Trim('"')

    $myJava = Join-Path $jenkinsHome "\jre\bin\java.exe"
    $cliWar = Join-Path $jenkinsHome "\war\WEB-INF\jenkins-cli.jar"
    $adminPassword = Get-Content (Join-Path $jenkinsHome "secrets/initialAdminPassword")
    Get-Content data\samplewebapi.xml | . $myJava -jar $cliWar -s http://localhost:8080 create-job samplewebapi --username admin --password $adminPassword
}

function Install-IIS {
    Import-Module ServerManager
    Add-WindowsFeature -Name Web-Server -IncludeAllSubFeature
    Add-WindowsFeature -Name Net-Framework-Features -IncludeAllSubFeature
    Add-WindowsFeature -Name Net-Framework-45-Features -IncludeAllSubFeature
    New-Item -Path "c:\inetpub" -type directory -Force -ErrorAction SilentlyContinue
    New-Item -Path "c:\inetpub\Log" -type directory -Force -ErrorAction SilentlyContinue
    New-Item -Path "c:\inetpub\WWWRoot" -type directory -Force -ErrorAction SilentlyContinue
}

function Bootstrap-Server {
    $tempWorkspace = New-TemporaryDirectory
    echo "Using temp workspace folder: $tempWorkspace ..."
    Install-Jenkins $jenkinsURL $tempWorkspace
    Jenkins-SeedJob
    Install-VSExpress $tempWorkspace
    Install-NUnit
    Install-Nuget
    Install-IIS

    #Nunit.xml to JUnit.xml translation script
    cp scripts/nunit-junit.xslt "C:\Program Files (x86)\Jenkins"

    #Our simple publish profile
    cp data/SampleWebAPI.pubxml "C:\Program Files (x86)\Jenkins"

    # Remove-Item $tempWorkspace -recurse
    # TODO: Cleanup tempWorkspace folder
}

#... and Go!
Bootstrap-Server
